import atexit
import argparse
import getpass
import sys
# import textwrap
import time
import ssl

from pyvim import connect
from pyVmomi import vim


def get_args():
    parser = argparse.ArgumentParser()

    # because -h is reserved for 'help' we use -s for service
    parser.add_argument('-s', '--host',
                        required=True,
                        action='store',
                        help='vSphere service to connect to')

    parser.add_argument('-u', '--user',
                        required=True,
                        action='store',
                        help='User name to use when connecting to host')

    parser.add_argument('-p', '--password',
                        required=False,
                        action='store',
                        help='Password to use when connecting to host')

    parser.add_argument('-n', '--name',
                        required=True,
                        action='store',
                        help='Name of the virtual_machine to look for.')

    parser.add_argument('-f', '--folder',
                        required=True,
                        action='store',
                        help='Name of the directory or folder in vSphere VM is currently in.')

    parser.add_argument('-d', '--directory',
                        required=True,
                        action='store',
                        help='Name of the directory or folder in vSphere to move VM to.')

    args = parser.parse_args()

    if not args.password:
        args.password = getpass.getpass(
            prompt='Enter password for host %s and user %s: ' %
                   (args.host, args.user))

    return args


args = get_args()


# form a connection...
def connect_to_vcenter():
    global args, si, content, vmname, vm_folder_name
    sslgrab = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    sslgrab.verify_mode = ssl.CERT_NONE
    si = connect.SmartConnect(host=args.host, user=args.user, pwd=args.password, sslContext=sslgrab)
    content = si.content
    vmname = args.name.upper()
    vmhost = args.host.upper()
    vm_folder_name = args.folder
    # doing this means you don't need to remember to disconnect your script/objects
    atexit.register(connect.Disconnect, si)
    print("\n\nYou have successfully connected to " + vmhost + "\n")


connect_to_vcenter()


# Method that populates objects of type vimtype
def get_all_objs(content, vimtype):
    obj = {}
    container = content.viewManager.CreateContainerView(content.rootFolder, vimtype, True)
    for managed_object_ref in container.view:
        obj.update({managed_object_ref: managed_object_ref.name})
    return obj


# Method that populates objects type foldertype
def get_all_folder_objs(content, foldertype):
    obj = {}
    container = content.viewManager.CreateContainerView(content.rootFolder, foldertype, True)
    for managed_object_ref in container.view:
        obj.update({managed_object_ref: managed_object_ref.name})
    return obj


# Calling above method
getAllVms = get_all_objs(content, [vim.VirtualMachine])

# Calling method above to get folders
getAllVmFolders = get_all_folder_objs(content, [vim.Folder])


def print_vm_folder():
    print("VM Folders:")
    for vmfolders in getAllVmFolders:
        print(vmfolders.name)
        if vmfolders.name.upper() == vm_folder_name.upper():
            print("")
            print("You have chosen VM folder: " + vm_folder_name)
            break


print_vm_folder()


# Iterating each vm object and selecting vm name from args
def print_vm():
    global vm, vmname
    for vm in getAllVms:
        # print(vm.name)
        if vm.name.upper() == vmname:
            vmname = vm
            print("You have selected: " + vm.name)
            break


print_vm()


def find_vm():
    global vm
    vm = None
    entity_stack = si.content.rootFolder.childEntity
    while entity_stack:
        entity = entity_stack.pop()

        if entity.name == args.name.upper():
            vm = entity
            del entity_stack[0:len(entity_stack)]
        elif hasattr(entity, 'childEntity'):
            entity_stack.extend(entity.childEntity)
        elif isinstance(entity, vim.Datacenter):
            entity_stack.append(entity.vmFolder)
        elif isinstance(entity, vim.Folder):
            # add all child entities from this folder to our search
            entity_stack.extend(entity.childEntity)
    if not isinstance(vm, vim.VirtualMachine):
        print("could not find a virtual machine with the name %s" % args.name)
        sys.exit(-1)
    print("Found VirtualMachine: %s Name: %s" % (vm, vm.name))


find_vm()


def move_vm():
    search_index = si.content.searchIndex
    new_vm_dir = search_index.FindByInventoryPath("CARTERCOMPUTE-DC/vm/" + args.directory)
    new_vm_dir.MoveInto([vmname])
    print("This VM has been moved to: " + args.directory)


def power_off_vm():
    global task
    if vm.runtime.powerState == vim.VirtualMachinePowerState.poweredOn:
        # using time.sleep we just wait until the power off action
        # is complete. Nothing fancy here.
        print("Powering off...")
        task = vm.PowerOff()
        while task.info.state not in [vim.TaskInfo.State.success,
                                      vim.TaskInfo.State.error]:
            time.sleep(1)
        print(args.name.upper() + " is powered off!")


# Power Off VM..
power_off_vm()


def power_on_vm():
    global task
    if vm.runtime.powerState != vim.VirtualMachinePowerState.poweredOn:
        # using time.sleep we just wait until the power on action
        # is complete. Nothing fancy here either.
        print("Powering on...")
        task = vm.PowerOn()
        while task.info.state not in [vim.TaskInfo.State.success,
                                      vim.TaskInfo.State.error]:
            time.sleep(1)
        print(args.name.upper() + " is powered on!")


# Power on VM
# power_on_vm()

if vm.runtime.powerState != vim.VirtualMachinePowerState.poweredOn:
    move_vm()
